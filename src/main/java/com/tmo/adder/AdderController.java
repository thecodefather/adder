package com.tmo.adder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class AdderController
{
    @Autowired
    private AdderServiceImpl adderService;

    @RequestMapping(method = RequestMethod.GET, path = "/{number1}/{number2}", produces = "application/json")
    @ApiOperation("Returns the sum of the 2 numbers")
    public ResponseEntity<Map<String, Integer>> addNumbers(@ApiParam("First number") @PathVariable int number1,
                                                           @ApiParam("Second number") @PathVariable int number2)
    {
        int addedNumbers = adderService.add(number1, number2);
        return new ResponseEntity<>(Collections.singletonMap("sum", addedNumbers), HttpStatus.OK);
    }
}

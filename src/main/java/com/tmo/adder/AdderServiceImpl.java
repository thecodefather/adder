package com.tmo.adder;

import org.springframework.stereotype.Service;

@Service
public class AdderServiceImpl
{
    public int add(int number1, int number2)
    {
        return number1 + number2;
    }
}
